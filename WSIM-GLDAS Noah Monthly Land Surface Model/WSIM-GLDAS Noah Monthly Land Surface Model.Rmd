---
title: "WSIM-GLDAS Noah Monthly Land Surface Model"

date: "`r Sys.Date()`"

submission-contact:
  - name: Cynthia Crowley
    email: crowley@isciences.com  
    affiliation: ISciences, LLC.

dataset-author: 
  - name: Dan Baston
    email: baston@isciences.com
    affiliation: ISciences, LLC.
  - name: Cynthia Crowley
    email: crowley@isciences.com
    affiliation: ISciences, LLC.

abstract: """The Water Security Indicator Model (WSIM) produces a land surface model-driven characterizes surpluses and deficits of fresh water across the global terrestrial surface in terms of return periods. Driven by outputs of NASA's Global Land Data Assimilation V2.0 monthly Noah Land Surface Model, WSIM generates anomalies of: fresh water deficits (composite index), fresh water surpluses (composite index), temperature, precipitation, soil moisture, potential minus actual evapotranspiration, runoff, and flow-accumulated runoff (total blue water), each with respect to a theoretical frequency distribution over a historical baseline period of January 1950 to December 2009. """

dataset-host: https://www.data.org
    
datatypes:
  - raster

  
spatial-information:
  extent: global
  resolution: quarter degree,
  coordinate-system: WGS1984

temporal-information:
  start-year: 1948
  end-year: 2010
  time-step: monthly

related-packages:
  - r package 1
  - r package 2
  - r package 3

vignettes:
  - Related Dante Vignette  

pros:
  - It's great for this
  - The best data in this country

cons:
  - The coverage is poor here
  - These methodologies are suspect
  
citekey: ISciencesLLC2015

bibliography: "wsim_references.bib"

output: 
  danteSubmit::danteDataset
  
---

### Discussion:

This data is really good, but there are a few criticisms. It's featured in these wonderful peer reviewed studies and commissioned reports. In addition to peer reviewed research, several tutorials and vignettes feature this data. There is a great R package that provides an interface to data API. This other package has functions designed to prepare and visualize this specific dataset or datasets just like it.

### Screenshot or Representative Figure:

![Figure 1: Appropriate caption that is both concise and informative.](screenshot.png)

This may also be developed from a code chunk:

```{r message=FALSE, warning=FALSE, dev='svg', fig.cap="Figure 2: Weight as a function of height for a subset of 15 women. Blue line represents linear relationship with 95% confidence interval (grey bands."}
library(ggplot2)
data(women)
women.scatter<-ggplot(women,aes(x=height,y=weight))+
  geom_point(size=3)+
  stat_smooth(method="lm", formula="y~x")+
  labs(title="Weight as a Function of Height for 15 Women",
       x="Height (Inches)",
       y="Weight (lbs.)")+
  theme_minimal()
women.scatter
  
```


### Citation:

@ISciencesLLC2015

### Additional Submission Comments:

These are some additional comments I would like to add that were not addressed above.

# Reference
